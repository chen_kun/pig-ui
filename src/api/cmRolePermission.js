
import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/content/cmRolePermission/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/content/cmRolePermission/',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/content/cmRolePermission/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/content/cmRolePermission/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/content/cmRolePermission/',
    method: 'put',
    data: obj
  })
}
