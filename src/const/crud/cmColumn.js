/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

const DIC = {
  vaild: [{
    label: '否',
    value: 'false'
  },
  {
    label: '是',
    value: 'true'
  }
  ]
}
export const tableOption = {
  'border': true,
  'indexLabel': '序号',
  'index': true,
  'stripe': true,
  'menuAlign': 'center',
  'align': 'center',
  'editBtn': false,
  'delBtn': false,
  'addBtn': false,
  'dic': [],
  'column': [{
    width: 150,
    label: '名称',
    prop: 'name',
    align: 'center',
    sortable: true,
    rules: [{
      required: true,
      message: '请输入名称',
      trigger: 'blur'
    }]
  }, {
    width: 300,
    label: '父级菜单',
    prop: 'parentId',
    align: 'center',
    sortable: true,
    rules: [{
      required: true,
      message: '请输入父级菜单',
      trigger: 'blur'
    }]
  }, {
    label: '代码',
    prop: 'code',
    align: 'center',
    width: 150,
    rules: [{
      required: true,
      message: '请输入scope',
      trigger: 'blur'
    }]
  }, {
    label: '主页连接',
    prop: 'homeUrl',
    align: 'center',
    width: 150,
    rules: [{
      required: true,
      message: '请输入主页连接',
      trigger: 'blur'
    }]
  },{
    label: 'id',
    prop: 'id',
    align: 'center',
    width: 150,
    hide: true,
    rules: [{
      required: true,
      message: '请输入主页连接',
      trigger: 'blur'
    }]
  }, {
    label: '模版连接',
    prop: 'template',
    align: 'center',
    width: 150,
  }, {
    label: '类型',
    prop: 'type',
    align: 'center',
    width: 150,
  }]
}
